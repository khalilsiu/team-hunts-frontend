import {IAuthState} from './states'
import {IAuthAction} from './actions'

const initialState: IAuthState = {
    isAuthenticated: (localStorage.getItem('token') != null)
}

export const authReducers = (state: IAuthState = initialState, actions: IAuthAction) =>{
    switch(actions.type){
        case 'LOGIN_SUCCESS':
            return {
                ...state,
                isAuthenticated: actions.isAuthenticated
            }
        case 'LOGIN_FAILURE':
            return {
                ...state,
                isAuthenticated: false
            }
        
        default:
            return state
    }
}