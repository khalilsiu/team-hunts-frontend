
// action is simply an object with an action type and the data needed
// these functions are action creators

export function loginSuccess(){
    return {
        type: "LOGIN_SUCCESS" as "LOGIN_SUCCESS",
        isAuthenticated: true
    }
}

export function loginFailure(){
    return {
        type: "LOGIN_FAILURE" as "LOGIN_FAILURE",
        isAuthenticated: false,
    }
}

// the union type of action, for convenience issue
type AuthActionsCreator = typeof loginSuccess | typeof loginFailure

export type IAuthAction = ReturnType<AuthActionsCreator>