import { Dispatch } from 'redux'
import { IAuthAction, loginSuccess, loginFailure } from './actions'
import { alertDisplay, IAlertAction } from '../alert/actions'
import { push, CallHistoryMethodAction } from "connected-react-router";
const { REACT_APP_API_SERVER } = process.env

export function login(email: string, password: string) {
  return async (dispatch: Dispatch<IAuthAction | IAlertAction | CallHistoryMethodAction>) => {
    const res = await fetch(`${REACT_APP_API_SERVER}/login`, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
      },
      body: JSON.stringify({
        email,
        password,
      }),
    })
    const response = await res.json()
    if (response.isSuccess) {
      localStorage.setItem('token', response.token)
      dispatch(loginSuccess())
      dispatch(push('/teams'))

    } else {
      dispatch(loginFailure())
      dispatch(alertDisplay('danger',response.error))
    }
  }
}
