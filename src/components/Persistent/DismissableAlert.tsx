import * as React from 'react'
import { Alert } from 'react-bootstrap'
import { useSelector, useDispatch } from 'react-redux'
import { IRootState } from '../../store'
import { IAlertState } from '../../alert/states'
import { alertDismiss } from '../../alert/actions'
import './DismissableAlert.css'


export default function DismissableAlert() {
  const errorState = useSelector<IRootState, IAlertState>((state) => state.alert)
  const dispatch = useDispatch()

  const handleClose = () => {
    dispatch(alertDismiss())
  }

  return errorState.alertType ? (
    <Alert className="dismissable-alert" variant={errorState.alertType} onClick={handleClose} dismissible>
      {errorState.msg}
    </Alert>
  ) : (
    <></>
  )
}
