import './Team.css'
import * as React from 'react'
import { Navbar, Nav, Card, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'

const teams = [
  {
    name: 'Sales and Analytics',
  },
  {
    name: 'Global Data',
  },
]

export default function Teams() {
  return (
    <div>
      <div className="vertical-navbar">
        <Link className="logo-link" to="/">
          Team Hunts
        </Link>
        <Link className="links" to="/">
          Teams
        </Link>
        <Link className="links" to="/">
          Teams
        </Link>
        <Link className="links" to="/">
          Teams
        </Link>
      </div>

      <Card style={{ width: '18rem' }}>
        <Card.Body>
          <Card.Title>Card Title</Card.Title>
          <Card.Text>
            Some quick example text to build on the card title and make up the bulk of the card's
            content.
          </Card.Text>
          <Button variant="primary">Go somewhere</Button>
        </Card.Body>
      </Card>
    </div>
  )
}
