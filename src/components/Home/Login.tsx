
import React from 'react'
import { Button, Form } from 'react-bootstrap'
import { Formik } from 'formik'
import * as yup from 'yup'
import { login } from '../../auth/thunks'
import { useDispatch } from 'react-redux'
import NavigationBar from './NavigationBar'





const schema = yup.object({
  email: yup.string().email().required().label('Email'),
  password: yup
    .string()
    .required()
    // .matches(
    //   /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
    //   'Must Contain 8 Characters, and a mix of number or symbols'
    // )
    .label('Password'),
})

function Login() {
  const dispatch = useDispatch();
  return (
    <div >
        <NavigationBar/>
          <Formik
            validationSchema={schema}
            onSubmit={(values) => {
              const {email, password} = values;
              dispatch(login(email, password));
            }}
            initialValues={{
              email: '',
              password: '',
            }}
          >
            {({ handleSubmit, handleChange, handleBlur, touched, errors, values }) => (
              <Form onSubmit={handleSubmit}>
                <Form.Group controlId="formBasicEmail">
                  <Form.Label>Email address</Form.Label>
                  <Form.Control
                    onChange={handleChange('email')}
                    type="email"
                    value={values.email}
                    placeholder="Enter email"
                    onBlur={handleBlur('email')}
                  />
                  {touched.email && errors.email}
                  <Form.Text className="text-muted">
                    Please login using your company email
                  </Form.Text>
                </Form.Group>

                <Form.Group controlId="formBasicPassword">
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    onChange={handleChange('password')}
                    type="password"
                    value={values.password}
                    placeholder="Password"
                    onBlur={handleBlur('password')}
                  />
                  {touched.password && errors.password}
                </Form.Group>
                <Button variant="primary" type="submit">
                  Submit
                </Button>
              </Form>
            )}
          </Formik>

    </div>
  )
}

export default Login
