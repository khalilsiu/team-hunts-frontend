import './Home.css'
import React from 'react'
import NavigationBar from './NavigationBar'

function Home() {
  return (
    <div >
      <NavigationBar/>
      <div className="text-container" >
        <h1 className="main-text">
          We make electrocars <br />
          more comfortable <br />
          for everyone
        </h1>
        <h6 className="sub-text">
          We provide mobile charging robots
          <br />
          for parking facilities enabling people <br />
          to charge with no waste of time
        </h6>
      </div>

      </div>
  )
}

export default Home
