
import React from 'react'
import { Navbar, Nav} from 'react-bootstrap'
import { Link } from 'react-router-dom'

const navItems = [
  {
    text: 'How it works',
    link: 'how-it-works',
  },
  {
    text: 'About us',
    link: 'about-us',
  },
  {
    text: 'Contact us',
    link: 'contact-us',
  },
  {
    text: 'Login',
    link: 'login',
  },
]



function NavigationBar() {
  return (
    <div >
      <Navbar className="nav" expand="lg">
        <Link className="nav-links" to="/">
          Team Hunts
        </Link>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto">
            {navItems.map((item, i) => (
              <Link
                to={'/' + item.link}
                className="nav-links"
                key={i}
              >
                {item.text}
              </Link>
            ))}
          </Nav>
        </Navbar.Collapse>
      </Navbar>
  

      </div>
  )
}

export default NavigationBar
