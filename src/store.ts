import {createStore, compose, combineReducers, applyMiddleware} from 'redux'
import {IAuthAction} from './auth/actions'
import { authReducers } from './auth/reducers';
import { IAuthState } from './auth/states';
import logger from 'redux-logger'
import {createBrowserHistory} from 'history'
import {RouterState, connectRouter, routerMiddleware} from 'connected-react-router'
import thunk, {ThunkDispatch} from 'redux-thunk'
import { alertReducers } from './alert/reducers';
import { IAlertState } from './alert/states';

export const history = createBrowserHistory()
// these are for compose, so that we can use more than one middleware
declare global{
    /* tslint:disable:interface-name */
    interface Window{
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__:any
    }
}
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// the state of the whole application
// combine state by composition
export interface IRootState{
    auth: IAuthState
    alert: IAlertState
    router: RouterState
}

// combine actions by union
type IRootAction = IAuthAction


// take in the old state as parameter, reduce the state based on action and return new state
// dispatch in components will dispatch the actions to be used to update states using the reducer
// using mapDispatchToProps
// combine resucers by the function combineReducers()
// connected-react-router allows us to do dynamic routing
// component renders when the route matches the current url vs static routing (express)
const rootReducer = combineReducers<IRootState>({
    auth: authReducers,
    alert: alertReducers,
    router: connectRouter(history)
})

// this is to ensure type safety
// async action creator that returns a function that takes dispatch as a parameter
export type Dispatch = ThunkDispatch<IRootState, null, IRootAction>

// createStore creates a globally accessible store from state, actions and reducers
// Action is the type of redux action

export default createStore<IRootState, IRootAction, {},{}>(
    rootReducer,
    composeEnhancers(
        applyMiddleware(logger),
        applyMiddleware(thunk),
        applyMiddleware(routerMiddleware(history))
    ));