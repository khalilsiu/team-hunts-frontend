export interface IAlertState{
    alertType:'primary'|
    'secondary'|
    'success'|
    'danger'|
    'warning'|
    'info'|
    'light'|
    'dark' | undefined
    msg: string
}