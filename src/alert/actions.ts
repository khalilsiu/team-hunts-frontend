// action is simply an object with an action type and the data needed
// these functions are action creators

export function alertDisplay(
  alertType:
    | 'primary'
    | 'secondary'
    | 'success'
    | 'danger'
    | 'warning'
    | 'info'
    | 'light'
    | 'dark'
    | undefined,
  msg: string
) {
  return {
    type: 'ALERT_DISPLAY' as 'ALERT_DISPLAY',
    alertType,
    msg,
  }
}

export function alertDismiss() {
  return {
    type: 'ALERT_DISMISS' as 'ALERT_DISMISS',
  }
}

// the union type of action, for convenience issue
type AlertActionsCreator = typeof alertDisplay | typeof alertDismiss

export type IAlertAction = ReturnType<AlertActionsCreator>
