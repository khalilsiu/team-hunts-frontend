import {IAlertState} from './states'
import {IAlertAction} from './actions'

const initialState: IAlertState = {
    alertType: undefined,
    msg:""
}

export const alertReducers = (state: IAlertState = initialState, actions: IAlertAction) =>{
    switch(actions.type){
        case 'ALERT_DISPLAY':
            return {
                ...state,
                alertType: actions.alertType,
                msg: actions.msg
            }
        case 'ALERT_DISMISS':
            return {
                ...state,
                alertType: undefined,
                msg: ""
            }
        default:
            return state
    }
}