import React from 'react'
import Landing from './components/Home/Home'
import Login from './components/Home/Login'
import store, {history} from './store'
import { Provider } from 'react-redux'
import 'bootstrap/dist/css/bootstrap.min.css'
import { ConnectedRouter } from 'connected-react-router'
import { Switch, Route } from 'react-router-dom'
import ApolloClient from 'apollo-boost'
import {ApolloProvider} from 'react-apollo'
import DismissableAlert from './components/Persistent/DismissableAlert'
import Teams from './components/Teams/Teams'
import { PrivateRoute } from './PrivateRoute'

//apollo client setup

const client = new ApolloClient({
  uri: 'http://localhost:8080/graphql'
})


function App() {
  return (
    <ApolloProvider client={client}>
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <div>
              <DismissableAlert/>
            <Switch>
              {/* without exact, it is going to match like startWith */}
              <Route path="/" exact={true} component={Landing}/>
              <Route path="/login" exact={true} component={Login}/>
              <PrivateRoute path="/teams" exact={true} component={Teams}/>
            </Switch>
          </div>
        </ConnectedRouter>
      </Provider>
    </ApolloProvider>
  )
}

export default App
