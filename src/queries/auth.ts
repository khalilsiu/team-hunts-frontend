import {gql} from 'apollo-boost'

export const loginMutation = gql`
    mutation($email: String!, $password: String!) {
        loginAdmin (email: $email, password: $password){
            token
        }
    }
`